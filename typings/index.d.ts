declare module 'require-all' {
  interface Options {
    dirname: string,
    filter?:  ((name: string, path: string) => string) | RegExp,
    excludeDirs?: RegExp,
    map?: (name: string, path: string) => string,
    resolve?: (module: any) => any,
    recursive?: true | false,
  }
  function rall(options: Options ) : {[key: string]: any};
  export = rall;
}

