import { UserDAO } from "../db/UserDAO";
import { Serializable } from "contracts/Serializable";
import Message from "./Message";
import MessageFactory from "factories/MessageFactory";
import MessageType from "enums/MessageType";
import { User } from "./User";
import winston from "winston";

export class Room extends Map<string, UserDAO> implements Serializable {
  constructor(public roomId: string = "") {
    super();
  }

  get id(): string {
    return `room_${this.roomId}`;
  }

  public toJson(): string {
    return JSON.stringify({
      roomId: this.id,
      users: [...this.values()]
    })
  }

  public toString(): string {
    return this.toJson();
  }

  public join(u: User, message?: Message): boolean {
    if(!message) {
      message = MessageFactory.create(
        MessageType.JOIN_ROOM,
        JSON.stringify(
          { 
            id: this.roomId,
            users: [...this.values()].map((u) => u.toJson()) 
          }
        )
      )
    }
    try {
      u.getSocket().send(message.toJson());
    } catch (e) {
      winston.error(e)
      return false;
    }
    return true;
  } 

  public leave(u: User): boolean {
    try{
      this.delete(u.id);
    } catch (e) {
      winston.error(e);
      return false;
    }
    return true;
  }

  public merge(room: Room): Room {
    for(const u of room.values()) {
      this.set(u.id, u);
    }
    return this;
  }

  public notifyRoom(user: User, message: Message): void {
    this.forEach(u => {
      if(u.id !== user.id) u.getSocket().send(message.toJson())
    });
  }

  public inRoom(user: User): boolean {
    return [...this.values()].some(u => u.id === user.id);
  }

}