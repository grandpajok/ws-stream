import { connection as redis } from "@db/connection";
import Storage from "./Storage";
import { Room } from "./Room";

export default class RoomStorage extends Storage<Room> {

  constructor();
  constructor(rooms: Room[], key?: string );
  constructor(rooms: Room[] = [], key = "") {
    super();
    if(rooms) {
      this.push(...rooms);
    }
    if(key) {
      this.storageId = key;
    } else {
      redis.randomkey(((err, k) => this.storageId = k));
    }
    
  }

  push(...items: Room[]) : number {
    for (const room of items) {
      redis.append(this.id, room.id)
      redis.set(room.id, room.toJson());
      const roomStored = this.find((r) => r.roomId === room.roomId);
      if(roomStored) {
        roomStored.merge(room);
      } else {
        super.push(room);
      }
    }
    return this.length;
  }

  get id(): string {
    return `storage_room_${this.storageId}`;
  }
}