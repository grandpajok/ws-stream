import { Serializable } from "contracts/Serializable";
import MessageType from "enums/MessageType";

export default class Message implements Serializable {
  public static MESSAGE_EVENT = 'message';

  constructor(public type: MessageType, public data: string) { }
  toJson(): string {
    return JSON.stringify({
      type: this.type,
      message: this.data,
    });
  }
}