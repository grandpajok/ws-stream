import { Serializable } from "contracts/Serializable";
import { Entity } from "contracts/Entity";
import { Client, Socket } from "socket.io";
import { UserDAO } from "../db/UserDAO";
import generateChars from "@utils/generateChars";

export class User implements Entity, Serializable {
  private _props: UserDAO;

  constructor(
    private socket: Socket,
    props: UserDAO | null = null
  ) {
    this._props = props ? props : new UserDAO(this.getClient().id, generateChars(5));
  }

  public getSocket(): Socket {
    return this.socket
  }

  public getClient(): Client {
    return this.getSocket().client;
  }

  
  
  get id() : string {
    return this.props.id;
  }

  get username(): string {
    return this.props.username;
  }

  set username(value: string) {
    this._props.username = value;
  }

  get props(): UserDAO {
    return this._props;
  }
 

  toJson(): string {
    return JSON.stringify({
      id: this.props.id,
      username: this.props.username
    });
  }
}