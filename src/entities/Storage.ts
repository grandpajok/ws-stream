import { connection as redis } from "@db/connection";

export default class Storage<T> extends Array<T> {

  public storageId = "";

  constructor() {
    super();
    redis.randomkey(((err, k) => this.storageId = k));
  }
}