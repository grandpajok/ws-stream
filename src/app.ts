import requireAll from 'require-all';
import SocketIo, { Socket } from 'socket.io';
import express from 'express';
import path from 'path';
import winston, { transports } from 'winston';
import dotenv from 'dotenv';
import { connection } from '@db/connection';
import { User } from 'entities/User';
import Storage from 'entities/Storage';
import { Room } from 'entities/Room';
import { UserDAO } from '@db/UserDAO';
import { promisify } from 'util';
import RoomStorage from 'entities/RoomStorage';
import MessageFactory from 'factories/MessageFactory';
import MessageType from 'enums/MessageType';

const app = express();
const io = SocketIo();

const userStorage = new Storage<User>();
const globalRoom = new Room('globalRoom');
const roomStorage = new RoomStorage([globalRoom]);

winston.add(new transports.Console({
  format: winston.format.simple()
}));

dotenv.config({
  path: process.env.NODE_PATH || process.cwd(),
});

io.on("connection", async (s: Socket) => {
  const userId = await promisify(connection.INCR.bind(connection))('userId');
  const user = new User(s, new UserDAO(userId.toString()));
  requireAll({
    dirname: path.resolve(__dirname, './listeners'),
    filter: /^.*\.ts$/,
    resolve: (module: any) => {
      module.default.handle(user);
    }
  });
  userStorage.push(user);
  connection.set(user.id, user.toJson());
  const msg = MessageFactory.create(
    MessageType.SEND_MESSAGE,
    JSON.stringify({id: userId, message: `User ${userId} connected`})
  );
  s.send(msg.toJson());
  winston.info(`Connected ${s.id}`);
});

const port = 8000;

const server = app.listen(port, () => {
  winston.info('Server listens on ' + port);
});

io.listen(server);

export { roomStorage, userStorage };