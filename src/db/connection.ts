import redis from "@utils/redis";
import env from "@utils/env";
import winston from "winston";

const connection = redis({
  host: env('REDIS_HOST', '172.31.48.1'),
  password: env('REDIS_PASSWORD', 'remoteremote'),
  port: env('REDIS_PORT', 6379),
});

connection.on("error", winston.error);
connection.on("connect", () => winston.info('Redis Connected'));

export { connection };