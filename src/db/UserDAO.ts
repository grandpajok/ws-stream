import { Serializable } from "contracts/Serializable";
import { Socket } from "socket.io";
import { userStorage } from "app";

export class UserDAO implements Serializable {

  constructor(
    public _id: string,
    public username: string = ""
  ) {}

  toJson(): string {
    return JSON.stringify({
      id: this.id,
      username: this.username
    });
  }

  get id(): string {
    return `user_${this._id}`
  }

  toString(): string {
    return this.toJson();
  }

  getSocket(): Socket {
    return userStorage.find(u => u.id === this.id)?.getSocket() as Socket;
  }
}