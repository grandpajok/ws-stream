import randomBetween from "./randomBetween";

export default function (count = 5) : string {
  const [min, max] = [97, 122];
  return Array.from({length: count},() => String.fromCharCode(randomBetween(min, max))).join('');
}