export default (min : number, max : number) : number => {
  const between = max - min;
  return Math.random() * between + min;
}