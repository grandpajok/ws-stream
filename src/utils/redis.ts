import redis, { RedisClient } from 'redis';

export default (options: redis.ClientOpts): RedisClient => {
  return redis.createClient(options);
}