import Message from "entities/Message";
import MessageType from "enums/MessageType";

export default class MessageFactory {
  static create(type: MessageType, data: string): Message {
    return new Message(type, data);
  }
  static createError(data: string, type = MessageType.ERROR_ARG): Message {
    return new Message(type, data);
  }
}