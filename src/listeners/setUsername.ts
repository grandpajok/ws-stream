import { Listener } from "../contracts/Listener";
import { User } from "entities/User";
import Message from "entities/Message";
import MessageFactory from "factories/MessageFactory";
import MessageType from "enums/MessageType";
import generateChars from "@utils/generateChars";

export default {
  events: ["set_usrm", "set_username"],
  handle(u: User) {
    this.events.forEach(e => u.getSocket().on(e, this.handler.bind(u)));
  },
  handler(...args: any[]) {
    let username = String(args[0]).trim();
    if(!username) {
      username = generateChars(5);
    }
    if(username.length > 150) {
      return this.getSocket().emit(
        Message.MESSAGE_EVENT,
        MessageFactory.create(MessageType.ERROR_ARG, "Wrong username").toJson()
      )
    }
    this.username = username;
    this.getSocket().emit(
      Message.MESSAGE_EVENT,
      MessageFactory.create(MessageType.USER, JSON.stringify(this.toJson())).toJson()
    );
  }
} as Listener