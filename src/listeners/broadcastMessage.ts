import { Listener } from "../contracts/Listener";
import { User } from "entities/User";
import { roomStorage, userStorage } from "app";
import Message from "entities/Message";
import MessageFactory from "factories/MessageFactory";
import MessageType from "enums/MessageType";

export default {
  events: ["broadcast", "bc_msg"],
  
  handle(u: User) {
    this.events.forEach(e => u.getSocket().on(e, this.handler.bind(u)));
  },

  handler(...args: any[]) {
    const roomId = args[0] ? args[0] : false;
    if(!roomId) return;
    const room = roomStorage.find(r => r.roomId === roomId);
    if(!room) return;
    if(!room.inRoom(this)) {
      return this.getSocket().emit(
        Message.MESSAGE_EVENT,
        MessageFactory.create(MessageType.ERROR_ARG, JSON.stringify({ error: "You must join to room" }))
      );
    }
    const message = args[1] ? args[1] : false;
    if(!message) {
      return this.getSocket().emit(
        Message.MESSAGE_EVENT,
        MessageFactory.create(MessageType.ERROR_ARG, JSON.stringify({ error: "Message is not defined" }))
      );
    }
    const msg = MessageFactory.create(MessageType.SEND_MESSAGE, JSON.stringify({ roomId: roomId, message }));
    room.notifyRoom(this, msg);
  }
} as Listener