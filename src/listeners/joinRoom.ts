import { Listener } from "../contracts/Listener";
import { User } from "entities/User";
import { roomStorage } from "app";
import Message from "entities/Message";
import MessageFactory from "factories/MessageFactory";
import MessageType from "enums/MessageType";
import { Room } from "entities/Room";
import generateChars from "@utils/generateChars";

export default {
  events: ["join_room"],
  
  handle(u: User) {
    this.events.forEach(e => u.getSocket().on(e, this.handler.bind(u)));
  },

  handler(...args: any[]) {
    const roomId = <string> (args[0] ? args[0] : Date.now().toString());
    let room = roomStorage.find((r) => r.roomId === roomId);
    if(room && room.inRoom(this)) {
      return this.getSocket().emit(
        Message.MESSAGE_EVENT,
        MessageFactory.create(MessageType.ERROR_ARG, JSON.stringify({ id: roomId, message: "Already in room" }))
      );
    }
    if(!this.username){
      this.username = args[1] ? args[1] : generateChars(5);
    }
    if(!room) {
      room = new Room(roomId);
      roomStorage.push(room);
      this.getSocket().emit(
        Message.MESSAGE_EVENT,
        MessageFactory.create(MessageType.CREATE_ROOM, JSON.stringify({ id: roomId }))
      );
    }
    if(!room.join(this)) return;
    const message = MessageFactory.create(
      MessageType.SEND_MESSAGE,
      JSON.stringify({ roomId: this.id,  message: `User ${this.username} joined to ${roomId}` })
    );
    room.notifyRoom(this, message);
  }
} as Listener