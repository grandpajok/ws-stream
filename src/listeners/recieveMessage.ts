import { Listener } from "../contracts/Listener";
import winston from 'winston';
import { User } from "entities/User";
import Message from "entities/Message";
import MessageFactory from "factories/MessageFactory";
import MessageType from "enums/MessageType";

export default {
  events: ["msg_get", "example"],
  handle(u: User) {
    this.events.forEach(e => u.getSocket().on(e, this.handler.bind(u)));
  },
  handler(...args: any[]) {
    winston.info(`Send message to ${this.getClient().id}`);
    this.getSocket().emit(
      Message.MESSAGE_EVENT,
      MessageFactory.create(
        MessageType.SEND_MESSAGE,
        JSON.stringify({ 'message': 'hello' + Math.random() })
      ).toJson()
    );
  }
} as Listener