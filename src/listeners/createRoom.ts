import { Listener } from "../contracts/Listener";
import { User } from "entities/User";
import { roomStorage } from "app";
import Message from "entities/Message";
import MessageFactory from "factories/MessageFactory";
import MessageType from "enums/MessageType";
import { Room } from "entities/Room";

export default {
  events: ["create_room"],
  
  handle(u: User) {
    this.events.forEach(e => u.getSocket().on(e, this.handler.bind(u)));
  },

  handler(...args: any[]) {
    const roomId = <string> args[0];
    const room = new Room(roomId ? roomId : Date.now().toString());
    roomStorage.push(room);
    this.getSocket().emit(
      Message.MESSAGE_EVENT,
      MessageFactory.create(MessageType.CREATE_ROOM, JSON.stringify({ id: roomId })).toJson()
    );
  }
} as Listener