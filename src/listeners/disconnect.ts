import { Listener } from "../contracts/Listener";
import { User } from "entities/User";
import { connection } from "@db/connection";

export default {
  events: ["disconnect"],
  handle(u: User) {
    this.events.forEach(e => u.getSocket().on(e, this.handler.bind(u)));
  },

  handler(...args: any[]) {
    connection.del(this.id);
    console.log(`User ${this.id} Disconnected `);
  }
} as Listener