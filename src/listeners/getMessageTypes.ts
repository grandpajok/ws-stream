import { Listener } from "../contracts/Listener";
import { User } from "entities/User";
import MessageType from "enums/MessageType";
import Message from "entities/Message";
import MessageFactory from "factories/MessageFactory";

export default {
  events: ["msg_types", "get_message_types", "get_msg_types"],

  handle(u: User) {
    this.events.forEach(e => u.getSocket().on(e, this.handler.bind(u)));
  },

  handler(...args: any[]) {
    const types = MessageType;

    this.getSocket().emit(
      Message.MESSAGE_EVENT,
      MessageFactory.create(MessageType.SYSTEM, JSON.stringify(types)).toJson()
    );
  }
} as Listener