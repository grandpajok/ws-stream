import { Listener } from "../contracts/Listener";
import { User } from "entities/User";
import Message from "entities/Message";
import MessageFactory from "factories/MessageFactory";
import MessageType from "enums/MessageType";

export default {
  events: ["get_usr", "get_user"],
  handle(u: User) {
    this.events.forEach(e => u.getSocket().on(e, this.handler.bind(u)));
  },
  handler(...args: any[]) {
    this.getSocket().emit(
      Message.MESSAGE_EVENT,
      MessageFactory.create(MessageType.USER, JSON.stringify(this.toJson())).toJson()
    );
  }
} as Listener