import { Listener } from "../contracts/Listener";
import { User } from "entities/User";
import { roomStorage } from "app";
import Message from "entities/Message";
import MessageType from "enums/MessageType";
import MessageFactory from "factories/MessageFactory";

export default {
  events: ["get_room"],

  handle(u: User) {
    this.events.forEach(e => u.getSocket().on(e, this.handler.bind(u)));
  },

  handler(...args: any[]) {
    const roomId = args[0];
    const room = roomStorage.find((r) => r.id == roomId);
    
    const data = room ? {
      id: roomId,
      users:[...room.values()].map((u) => u.toJson())
    } : {
      id: roomId,
      users: []
    };
    this.getSocket().emit(
      Message.MESSAGE_EVENT,
      MessageFactory.create(MessageType.GET_ROOM, JSON.stringify(data)).toJson()
    );
  }
} as Listener