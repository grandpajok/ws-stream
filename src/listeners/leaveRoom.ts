import { Listener } from "../contracts/Listener";
import { User } from "entities/User";
import { roomStorage } from "app";
import Message from "entities/Message";
import MessageFactory from "factories/MessageFactory";
import MessageType from "enums/MessageType";

export default {
  events: ["leave_room", "lv_room"],
  
  handle(u: User) {
    this.events.forEach(e => u.getSocket().on(e, this.handler.bind(u)));
  },

  handler(...args: any[]) {
    const roomId = args[0];
    const room = roomStorage.find((r) => r.roomId === roomId);
    if(!room) {
      return this.getSocket().emit(
        Message.MESSAGE_EVENT,
        MessageFactory.create(
          MessageType.ERROR_ARG,
          JSON.stringify({id: roomId, message: 'You are not in this room'})
        ).toJson()
      );
    }
    const msg = MessageFactory.create(
      MessageType.LEAVE_ROOM,
      JSON.stringify({id: roomId, message: `User ${this.username} Left room`})
    );
    if(!room.leave(this)) return;
    room.notifyRoom(this, msg)
  }
} as Listener