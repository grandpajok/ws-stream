enum MessageType {
  LEAVE_ROOM = 0xabc,
  JOIN_ROOM,
  GET_ROOM,
  DISCONNECT,
  SEND_MESSAGE,
  CREATE_ROOM,
  USER,
  SYSTEM,
  ERROR_ARG
}
export default MessageType;