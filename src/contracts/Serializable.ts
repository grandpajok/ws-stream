export interface Serializable {
  toJson() : string;
  toString?() : string;
}