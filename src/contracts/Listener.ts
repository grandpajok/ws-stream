import { User } from "entities/User";

export interface Listener {
  events: string[];
  handle(s: User): void
  handler(this: User, ...args: any[]): void; 
}